import  nltk
import numpy as np
# punkt is package with a pre tockenizer
# this download is necessary only if is the first time you run the code
#nltk.download('punkt')
from nltk.stem.porter import PorterStemmer
stemmer = PorterStemmer()

#splits a string in words units
def tokenize(sentence):
    return nltk.word_tokenize(sentence)

#generate the root form of the words
def stem(word):
    return stemmer.stem(word.lower())

#result of the two steps above
#this will feed the binary vector
def bag_of_words(tokenized_sentence, all_words):
    tokenized_sentence = [stem(w) for w in tokenized_sentence]
    bag = np.zeros(len(all_words), dtype=np.float32)
    for idx, w in enumerate(all_words):
        if w in tokenized_sentence:
            bag[idx] = 1.0
    return bag



