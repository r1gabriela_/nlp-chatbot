import torch
import torch.nn as nn


class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(NeuralNet, self).__init__()
        #input is the bag_of_words that feeds the net - X
        #first layer of neural net has the number of patterns(input_size)
        #middle has hidden layer
        #output layer has number of diff classes
        #after, apply softmax and get probabilities for each class - y
        self.l1 = nn.Linear(input_size, hidden_size)
        self.l2 = nn.Linear(hidden_size, hidden_size)
        self.l3 = nn.Linear(hidden_size, num_classes)
        #activation function
        self.relu = nn.ReLU()


    def forward(self, x):
        out = self.l1(x)
        out = self.relu(out)
        out = self.l2(out)
        out = self.relu(out)
        out = self.l3(out)
        #no activation and no softmax. Later will apply crossEntropyLoss
        return out

print('file works')
