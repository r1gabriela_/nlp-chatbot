import json
from nltk_utils import tokenize ,stem, bag_of_words
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from model import NeuralNet

#load json file
#'r' arg goes for read mode
with open('intents.json','r') as i:
    intents = json.load(i)

all_words = []
tags = []
xy = []

#xy matrix receive unit words(tokeneized) related to their tag
for intent in intents['intents']:
    tag = intent['tag']
    tags.append(tag)
    for pattern in intent['patterns']:
        w = tokenize(pattern)
        all_words.extend(w)
        xy.append((w, tag))

ignore_words  = ['?','!','.',',']
all_words = [stem(w) for w in all_words if w not in ignore_words]
#sorted(set()) remove duplicated words and tags
all_words = sorted(set(all_words))
tags = sorted(set(tags))

#code below create bag of words
X_train = []
y_train = []
for (pattern_sentence, tag) in xy:
    bag = bag_of_words(pattern_sentence, all_words)
    X_train.append(bag)

    label = tags.index(tag)
    y_train.append(label) #CrossEntropyLoss

X_train = np.array(X_train)
y_train = np.array(y_train)

#Hyperparameters
batch_size = 8
hidden_size = 8
output_size = len(tags)
input_size = len(X_train[0]) # is the length of each bag of words created, which has the same length of all_words. Is the same of X_train[0] (idx does not matter, they have same size)
learning_rate = 0.001
num_epochs = 1000 #can try diff ones here

#create new DataSet
class ChatDataset(Dataset):


    def __init__(self):
        self.n_samples = len(X_train)
        self.x_data = X_train
        self.y_data = y_train

    #later we can access with index dataset[idx]
    def __getitem__(self, index):
        return self.x_data[index], self.y_data[index]

    def __len__(self):
        return self.n_samples


dataset = ChatDataset()
#create data load from the dataset
#num_workers make the loading faster. If error set to 0
train_loader = DataLoader(dataset=dataset, batch_size=batch_size, shuffle=True, num_workers=2)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = NeuralNet(input_size, hidden_size, output_size).to(device) #push to device

#loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

#train the model
for epoch in range(num_epochs):
    for(words, labels) in train_loader:
        words = words.to(device)
        labels = labels.to(dtype=torch.long).to(device)


        #foward
        outputs = model(words)
        loss = criterion(outputs, labels)

        #backward and optimezer step
        #empty the gradients
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    if (epoch +1) % 100 == 0:
        print (f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')

print(f'final loss: {loss.item():.4f}')

#save data
data = {
"model_state": model.state_dict(),
"input_size": input_size,
"hidden_size": hidden_size,
"output_size": output_size,
"all_words": all_words,
"tags": tags
}

FILE = "data.pth"
torch.save(data, FILE)

print(f'training complete. file saved to {FILE}')
